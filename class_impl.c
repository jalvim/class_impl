#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE_STR	20
// #define SETTER_TEST	1

typedef struct _Class {
	void (*set)  	(struct _Class *, char *);
	void (*destroy) (struct _Class *);
	void (*print)   (struct _Class *);
	char *field;
	int  length;
} Class;

void set_class (Class *node, char *str) {
	strcpy(node->field, str);
	node->length = strlen(str);
}

void destroy_class (Class *node) {
	free(node->field);
	free(node);
}

void print_class (Class *node) {
	printf(
		"Conteúdo da classe: %s; \t tamanho: %d\n", 
		node->field, 
		node->length
	);
}

Class *newClass (char *str) {
	Class *node 	= (Class *) malloc(sizeof(Class));
	node->set 	= set_class;
	node->destroy 	= destroy_class;
	node->print 	= print_class;
	node->field 	= (char  *) malloc(SIZE_STR*sizeof(char));
	node->length 	= strlen(str);
	strcpy(node->field, str);

	return node;
}

int main (int argc, char **args) {
	int num_arg = argc - 1;
	if (argc == 1) {
		fprintf(stderr, "ERRO: Número insuficiente de argumentos.\n");
		return 1;
	}

	Class *nodes[num_arg];
	for (int i = 1; i < argc; i++)
		nodes[i-1] = newClass(args[i]);

#ifdef 	SETTER_TEST
	printf("~~~~~~~ INÍCIO DO TESTE ~~~~~~~\n");
	nodes[num_arg - 1]->print(nodes[num_arg - 1]);
	nodes[num_arg - 1]->set(nodes[num_arg - 1], "TROCADO");
	nodes[num_arg - 1]->print(nodes[num_arg - 1]);
	printf("~~~~~~~ FIM DO TESTE ~~~~~~~\n");
	printf("\n");
#endif
	for (int i = 0; i < num_arg; i++) {
		nodes[i]->print(nodes[i]);
		nodes[i]->destroy(nodes[i]);
	}

	printf("Fim de Programa.\n");
	return 0;
}
